package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class T_04 {
    public static void main(String[] args) throws IOException {
        //Scanner scan = new Scanner(System.in);
        File file=new File("t_04.txt");
        PrintWriter printWriter=new PrintWriter(file);
        ArrayList arrayList=new ArrayList<Integer>();
        Random random=new Random();

        int number=1,z_n=0,f_n=0,count=1;
        while(number!=0&&count<=20){
            //System.out.print("請輸入多筆整數(輸入0為結束):");
            printWriter.println(random.nextInt(32678 + 32678) - 32678);

            number=random.nextInt(32678+32678)-32678;
            while(count<10&&number==0) {
                number = random.nextInt(32678 + 32678) - 32678;
            }
            //System.out.println(number);
            //number=scan.nextInt();
            arrayList.add(number);
            count++;

        }

        printWriter.close();
        FileReader fileReader=new FileReader(file);
        Scanner inf = new Scanner(fileReader);

        int sum=0;
        number=0;
        fileReader.ready();

        for(int i=0;i<arrayList.size();i++){
            number=inf.nextInt();
            System.out.println(number);
            sum+=number;
            if ((Integer) arrayList.get(i)>0) z_n++;
            else if((Integer) arrayList.get(i)!=0)f_n++;
        }

        fileReader.close();

        System.out.print("負數個數 : "+f_n+"  正數個數 : "+z_n+"  加總 : "+number);
        System.out.println("  平均 : "+sum/(arrayList.size()-1));
    }
}
