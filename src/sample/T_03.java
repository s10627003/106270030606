package sample;

import java.util.ArrayList;
import java.util.Scanner;

public class T_03{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList arrayList=new ArrayList<Integer>();
        int number=1,z_n=0,f_n=0;
        while(number!=0){
            System.out.println("請輸入多筆整數(輸入0為結束):");
            number=scan.nextInt();
            arrayList.add(number);
        }
        for(int i=0;i<arrayList.size();i++){
            number+=(Integer) arrayList.get(i);
            if ((Integer) arrayList.get(i)>0) z_n++;
            else if((Integer) arrayList.get(i)!=0)f_n++;
        }
        System.out.print("負數個數 : "+f_n+"  正數個數 : "+z_n+"  加總 : "+number);
        System.out.println("  平均 : "+number/(arrayList.size()-1));
    }

}
