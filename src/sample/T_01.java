package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Scanner;

public class T_01 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        //launch(args);
        Scanner scan = new Scanner(System.in);
        // 从键盘接收数据
        int i = 0;
        float f = 0.0f;

        System.out.print("請輸入一個介於 100 - 999 的數字 : ");
        i = scan.nextInt();
        if (100 <= i && i <= 999) {
            // 判断输入的是否是整数
            int k=i,num=0;
            System.out.println("整数数据：" + i);
            while(k!=0){
                num+=k%10;
                k/=10;
            }
            System.out.println("和：" + num);
            k=i;num=1;
            while(k!=0){
                num*=k%10;
                k/=10;
            }
            System.out.println("積：" + num);
            k=i;num=0;
            num+=k/100;
            k=k%100;
            num-=k/10;
            k=k%10;
            num-=k;
            System.out.println("差：" + num);

        } else {
            // 输入错误的信息
            System.out.println("输入錯誤！");
        }
    }
}
